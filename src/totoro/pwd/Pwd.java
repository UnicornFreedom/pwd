package totoro.pwd;

import javax.swing.*;

public class Pwd {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            MainFrame frame = new MainFrame();
            frame.setSize(1024,100);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setLocationRelativeTo(null);
            frame.setTitle("PWD Detector");
            frame.setVisible(true);
        });
    }
}
