package totoro.pwd;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MainFrame extends JFrame {
    public MainFrame() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JLabel label1 = new JLabel();
        label1.setAlignmentX(Component.CENTER_ALIGNMENT);
        label1.setText(getPath1());
        panel.add(label1);
        JLabel label2 = new JLabel();
        label2.setAlignmentX(Component.CENTER_ALIGNMENT);
        label2.setText(getPath2());
        panel.add(label2);
        JLabel label3 = new JLabel();
        label3.setAlignmentX(Component.CENTER_ALIGNMENT);
        label3.setText(getPath3());
        panel.add(label3);
        add(panel);
    }

    private String getPath1() {
        return System.getProperty("user.dir");
    }

    private String getPath2() {
        Path currentRelativePath = Paths.get("");
        return currentRelativePath.toAbsolutePath().toString();
    }

    private String getPath3() {
        return (new File("./")).getAbsolutePath();
    }
}
